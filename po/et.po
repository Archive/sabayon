# Sabayon'i eesti keele tõlge.
# Estonian translation of Sabayon.
#
# Copyright (C) 2006-2010 The GNOME Project.
# This file is distributed under the same license as the sabayon package.
#
# Ivar Smolin <okul linux ee>, 2006-2010.
# Priit Laes <amd store20 com>, 2006, 2007.
# Mattias Põldaru <mahfiaz@gmail.com>, 2007, 2009, 2012.
#
msgid ""
msgstr ""
"Project-Id-Version: sabayon MASTER\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?"
"product=sabayon&component=general\n"
"POT-Creation-Date: 2010-12-04 12:08+0000\n"
"PO-Revision-Date: 2012-02-29 10:42+0200\n"
"Last-Translator: Mattias Põldaru <mahfiaz@gmail.com>\n"
"Language-Team: Estonian <>\n"
"Language: et\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bits\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"

msgid "Program to establish and edit profiles for users"
msgstr "Programm kasutajaprofiilide loomiseks ja muutmiseks"

msgid "translator-credits"
msgstr ""
"Ivar Smolin <okul linux ee>, 2006-2010.\n"
"Priit Laes <amd store20 com>, 2006, 2007.\n"
"Mattias Põldaru <mahfiaz gmail com>, 2007, 2009."

#, python-format
msgid "Changes in profile %s"
msgstr "Muudatused profiilis %s"

msgid "Ignore"
msgstr "Eira"

msgid "Lock"
msgstr "Lukusta"

msgid "Description"
msgstr "Kirjeldus"

#, python-format
msgid "Profile %s"
msgstr "Profiil %s"

msgid "_Profile"
msgstr "_Profiil"

msgid "_Save"
msgstr "_Salvesta"

msgid "Save profile"
msgstr "Profiili salvestamine"

msgid "_Close"
msgstr "Su_lge"

msgid "Close the current window"
msgstr "Käesoleva akna sulgemine"

msgid "_Edit"
msgstr "_Redaktor"

msgid "_Delete"
msgstr "_Kustuta"

msgid "Delete item"
msgstr "Kirje kustutamine"

msgid "_Help"
msgstr "A_bi"

msgid "_Contents"
msgstr "_Sisukord"

msgid "Help Contents"
msgstr "Abiteabe sisukord"

msgid "_About"
msgstr "_Programmist lähemalt"

msgid "About Sabayon"
msgstr "Sabayon'ist lähemalt"

msgid "Source"
msgstr "Allikas"

msgid "GConf"
msgstr "GConf"

msgid "Files"
msgstr "Failid"

msgid "Panel"
msgstr "Paneel"

#, python-format
msgid "Profile file: %s"
msgstr "Profiilifail: %s"

msgid "<no type>"
msgstr "<tüüp puudub>"

msgid "<no value>"
msgstr "<väärtus puudub>"

msgid "string"
msgstr "sõne"

msgid "integer"
msgstr "täisarv"

msgid "float"
msgstr "ujukoma"

msgid "boolean"
msgstr "tõeväärtus"

msgid "schema"
msgstr "skeem"

msgid "list"
msgstr "loend"

msgid "pair"
msgstr "paar"

#, python-format
msgid "Profile settings: %s"
msgstr "Profiili sätted: %s"

msgid "Name"
msgstr "Nimi"

msgid "Type"
msgstr "Tüüp"

msgid "Value"
msgstr "Väärtus"

#, python-format
msgid "Groups for profile %s"
msgstr "Profiili %s grupid"

msgid "Group"
msgstr "Grupp"

msgid "Use This Profile"
msgstr "Kasutab seda profiili"

#.
#. Translators: this string specifies how a profile
#. name is concatenated with an integer
#. to form a unique profile name e.g.
#. "Artist Workstation (5)"
#.
#, python-format
msgid "%s (%s)"
msgstr "%s (%s)"

msgid ""
"Your account does not have permissions to run the Desktop User Profiles tool"
msgstr ""
"Sinu kontol pole Töölauakasutajate profiilide tööriista käivitamiseks "
"piisavalt õiguseid"

msgid ""
"Administrator level permissions are needed to run this program because it "
"can modify system files."
msgstr ""
"Selle programmi käivitamiseks peavad olema halduri taseme õigused kuna see "
"programm võib muuta süsteemseid faile."

msgid "Desktop User Profiles tool is already running"
msgstr "Tööriist töölauakasutajate profiilide redigeerimiseks juba töötab."

msgid ""
"You may not use Desktop User Profiles tool from within a profile editing "
"session"
msgstr ""
"Sa ei tohi töölauakasutajate profiilide tööriista kasutada profiilide "
"redigeerimise seansis."

#, c-format
msgid "User account '%s' was not found"
msgstr "Kasutajakontot '%s' ei leitud"

#, c-format
msgid ""
"Sabayon requires a special user account '%s' to be present on this computer. "
"Try again after creating the account (using, for example, the 'adduser' "
"command)"
msgstr ""
"Sabayon'il on vaja, et selles arvutis oleks eriotstarbeline kasutajakonto "
"'%s'. Proovi pärast konto loomist uuesti (konto saab luua näiteks 'adduser' "
"käsuga)."

#, c-format
msgid ""
"A fatal error has occurred.  You can help us fix the problem by sending the "
"log in %s to %s"
msgstr ""
"Esines pöördumatu viga. Sa võid vea parandamisele kaasa aidata saates "
"logifaili %s arendajate aadressile %s"

#, c-format
msgid ""
"Sabayon will now exit.  There were some recoverable errors, and you can help "
"us debug the problem by sending the log in %s to %s"
msgstr ""
"Sabayon lõpetab töö. Programmis esines mõningaid probleeme ning soovitav "
"oleks nende vigade parandamiseks saata logifailid asukohas %s arendajate "
"aadressile %s."

msgid "Please use -h for usage options"
msgstr "Kasutusspikri vaatamiseks kasuta palun võtit -h"

#, c-format
msgid "No profile for user '%s' found\n"
msgstr "Kasutajale '%s' ei leitud profiili\n"

#, c-format
msgid "Usage: %s <profile-name> <profile-path> <display-number>\n"
msgstr "Kasutamine: %s <profiili-nimi> <profiili-rada> <kuva-number>\n"

msgid "Establish and Edit Profiles for Users"
msgstr "Kasutajaprofiilide loomine ja muutmine"

msgid "User Profile Editor"
msgstr "Kasutajaprofiili redaktor"

msgid "Add Profile"
msgstr "Profiili lisamine"

msgid "Profile _name:"
msgstr "Profiili _nimi:"

msgid "Use this profile for _all users"
msgstr "Seda profiili _kasutatakse kõigi kasutajate jaoks"

msgid "_Base on:"
msgstr "Millel _põhineb:"

msgid "_Details"
msgstr "Ü_ksikasjad"

msgid "_Groups"
msgstr "_Grupid"

msgid "_Groups:"
msgstr "_Grupid:"

msgid "_Profiles:"
msgstr "_Profiilid:"

msgid "_Users"
msgstr "Kas_utajad"

msgid "_Users:"
msgstr "Kas_utajad:"

msgid "Close _Without Saving"
msgstr "Sulge _ilma salvestamata"

#, python-format
msgid "Save changes to profile \"%s\" before closing?"
msgstr "Kas enne sulgemist salvestada profiili \"%s\" muudatused?"

msgid "If you don't save, all changes will be permanently lost."
msgstr "Kui Sa ei salvesta, siis lähevad kõik muudatused jäädavalt kaotsi."

#, python-format
msgid "Editing profile %s"
msgstr "Profiili %s redigeerimine"

msgid "_Quit"
msgstr "_Lõpeta"

msgid "_Changes"
msgstr "_Muudatused"

msgid "Edit changes"
msgstr "Muudatuste redigeerimine"

msgid "_Lockdown"
msgstr "_Lukustatud"

msgid "Edit Lockdown settings"
msgstr "Lukustussätete muutmine"

msgid "Enforce Mandatory"
msgstr "Määra kohustuslikuks"

msgid "Enforce mandatory settings in the editing session"
msgstr "Selles redigeerimisseansis kohustuslike sätete pealesurumine"

#, python-format
msgid "Lockdown settings for %s"
msgstr "Lukustussätted %s jaoks"

#, python-format
msgid ""
"There was a recoverable error while applying the user profile '%s'.  You can "
"report this error now or try to continue editing the user profile."
msgstr ""
"Esines kergemat sorti viga kasutajaprofiili '%s' rakendamisel. Sa võid "
"raporteerida veast kohe või jätkata kasutajaprofiili muutmist."

msgid "_Report this error"
msgstr "_Teata sellest veast"

msgid "_Continue editing"
msgstr "_Jätka redigeerimist"

#, python-format
msgid "Users for profile %s"
msgstr "Profiili %s kasutajad"

msgid "Unable to find a free X display"
msgstr "X'i vaba kuva pole võimalik leida"

msgid "Failed to start Xephyr: timed out waiting for USR1 signal"
msgstr ""
"Tõrge Xephyr-i käivitamisel: USR1 signaali ootamisel ületati ajapiirang"

msgid "Failed to start Xephyr: died during startup"
msgstr "Xephyr-i käivitamine nurjus: suri käivitamisel"

#. mprint ("========== BEGIN SABAYON-APPLY LOG (RECOVERABLE) ==========\n"
#. "%s\n"
#. "========== END SABAYON-APPLY LOG (RECOVERABLE) ==========",
#. stderr_str)
#, python-format
msgid ""
"There was a recoverable error while applying the user profile from file '%s'."
msgstr "Kasutaja profiilifaili '%s' rakendamisel tekkis kõrvaldatav viga."

#. mprint ("========== BEGIN SABAYON-APPLY LOG (FATAL) ==========\n"
#. "%s\n"
#. "========== END SABAYON-APPLY LOG (FATAL) ==========",
#. stderr_str)
#, python-format
msgid "There was a fatal error while applying the user profile from '%s'."
msgstr "Kasutaja profiilifaili '%s' rakendamisel tekkis fataalne viga."

msgid "Directory"
msgstr "Kataloog"

msgid "Link"
msgstr "Viit"

msgid "File"
msgstr "Fail"

#, python-format
msgid "%s '%s' created"
msgstr "%s '%s' loodud"

#, python-format
msgid "%s '%s' deleted"
msgstr "%s '%s' kustutatud"

#, python-format
msgid "%s '%s' changed"
msgstr "%s '%s' muudetud"

msgid "Applications menu"
msgstr "Rakenduste menüü"

msgid "Settings menu"
msgstr "Sätete menüü"

msgid "Server Settings menu"
msgstr "Serveri sätete menüü"

msgid "System Settings menu"
msgstr "Süsteemi sätete menüü"

msgid "Start Here menu"
msgstr "Alusta siit, menüü"

#, python-format
msgid "GConf key '%s' unset"
msgstr "GConf võti '%s' ei ole seatud"

#, python-format
msgid "GConf key '%s' set to string '%s'"
msgstr "GConf võtme '%s' väärtuseks määrati sõne '%s'"

#, python-format
msgid "GConf key '%s' set to integer '%s'"
msgstr "GConf võtme '%s' väärtuseks määrati täisarv '%s'"

#, python-format
msgid "GConf key '%s' set to float '%s'"
msgstr "GConf võtme '%s' väärtuseks määrati arvväärtus '%s'"

#, python-format
msgid "GConf key '%s' set to boolean '%s'"
msgstr "GConf võtme '%s' väärtuseks määrati tõeväärtus '%s'"

#, python-format
msgid "GConf key '%s' set to schema '%s'"
msgstr "GConf võtme '%s' väärtuseks määrati skeem '%s'"

#, python-format
msgid "GConf key '%s' set to list '%s'"
msgstr "GConf võtme '%s' väärtuseks määrati loend '%s'"

#, python-format
msgid "GConf key '%s' set to pair '%s'"
msgstr "GConf võtme '%s' väärtuseks määrati paar '%s'"

#, python-format
msgid "GConf key '%s' set to '%s'"
msgstr "GConf võtme '%s' väärtuseks määrati '%s'"

msgid "Default GConf settings"
msgstr "GConf'i vaikimisi sätted"

msgid "Mandatory GConf settings"
msgstr "GConf'i kohustuslikud sätted"

#, python-format
msgid "Mozilla key '%s' set to '%s'"
msgstr "Mozilla uue võtme '%s' väärtus on '%s'"

#, python-format
msgid "Mozilla key '%s' unset"
msgstr "Mozilla võti '%s' ei ole seatud"

#, python-format
msgid "Mozilla key '%s' changed to '%s'"
msgstr "Mozilla võtme '%s' väärtuseks määrati '%s'"

msgid "Web browser preferences"
msgstr "Veebisirvija eelistused"

msgid "Web browser bookmarks"
msgstr "Veebisirvija järjehoidjad"

msgid "Web browser profile list"
msgstr "Veebisirvija profiilide loend"

#, python-format
msgid "File Not Found (%s)"
msgstr "Faili ei leitud (%s)"

#, python-format
msgid "duplicate name(%(name)s) in section %(section)s"
msgstr "korduvad nimed (%(name)s) osas %(section)s"

#, python-format
msgid "redundant default in section %s"
msgstr "liiane vaikeväärtus sektsioonis %s"

msgid "no default profile"
msgstr "vaikimisi profiili pole"

#, python-format
msgid "Mozilla bookmark created '%s' -> '%s'"
msgstr "Mozilla järjehoidja on loodud: '%s' -> '%s'"

#, python-format
msgid "Mozilla bookmark folder created '%s'"
msgstr "Mozilla järjehoidjate kaust '%s' loodud"

#, python-format
msgid "Mozilla bookmark deleted '%s'"
msgstr "Mozilla järjehoidja '%s' on kustutatud"

#, python-format
msgid "Mozilla bookmark folder deleted '%s'"
msgstr "Mozilla järjehoidjate kaust '%s' on kustutatud"

#, python-format
msgid "Mozilla bookmark changed '%s' '%s'"
msgstr "Mozilla järjehoidja '%s' on muudetud: '%s'"

#, python-format
msgid "Mozilla bookmark folder changed '%s'"
msgstr "Mozilla järjehoidjate uus kaust on '%s'"

#, python-format
msgid "Panel '%s' added"
msgstr "Paneel '%s' lisatud"

#, python-format
msgid "Panel '%s' removed"
msgstr "Paneel '%s' eemaldatud"

#, python-format
msgid "Applet '%s' added"
msgstr "Rakend '%s' lisatud"

#, python-format
msgid "Applet '%s' removed"
msgstr "Rakend '%s' eemaldatud"

#, python-format
msgid "Object '%s' added"
msgstr "Objekt '%s' lisatud"

#, python-format
msgid "Object '%s' removed"
msgstr "Objekt '%s' eemaldatud"

#. Translators: This is a drawer in gnome-panel (where you can put applets)
msgid "Drawer"
msgstr "Sahtel"

msgid "Main Menu"
msgstr "Peamenüü"

#, python-format
msgid "%s launcher"
msgstr "Käivitaja: %s"

msgid "Lock Screen button"
msgstr "Ekraani lukustamise nupp"

msgid "Logout button"
msgstr "Väljalogimise nupp"

msgid "Run Application button"
msgstr "Rakenduse käivitamise nupp"

msgid "Search button"
msgstr "Otsingunupp"

msgid "Force Quit button"
msgstr "Jõuga lõpetamise nupp"

msgid "Connect to Server button"
msgstr "Serveriga ühendumise nupp"

msgid "Shutdown button"
msgstr "Seiskamise nupp"

msgid "Screenshot button"
msgstr "Ekraanipildi nupp"

msgid "Unknown"
msgstr "Tundmatu"

msgid "Menu Bar"
msgstr "Menüüriba"

msgid "Panel File"
msgstr "Paneelifail"

#, python-format
msgid "Failed to read file '%s': %s"
msgstr "Tõrge faili '%s' lugemisel: %s"

#, python-format
msgid "Failed to read metadata from '%s': %s"
msgstr "Tõrge metaandmete lugemisel '%s'-st: %s"

#, python-format
msgid "Invalid metadata section in '%s': %s"
msgstr "Vigane metateabe sektsioon failis '%s': %s"

#. change the raise to a dprint, since some files seem to disappear.
#. raise ProfileStorageException (_("Cannot add non-existent file '%s'") % src_path)
#, python-format
msgid "Cannot add non-existent file '%s'"
msgstr "Olematut faili '%s' pole võimalik lisada"

#, python-format
msgid "Couldn't rmdir '%s'"
msgstr "Kataloogi '%s' pole võimalik kustutada"

#, python-format
msgid "Couldn't unlink file '%s'"
msgstr "Faili '%s' pole võimalik kustutada"

#, python-format
msgid "Profile is read-only %s"
msgstr "Profiil %s on ainult lugemisõigustega"

#. Translators: You may move the "%(setting)s" and "%(np)s" items as you wish, but
#. do not change the way they are written.  The intended string is
#. something like "invalid type for setting blah in /ldap/path/to/blah"
#, python-format
msgid "invalid type for setting %(setting)s in %(np)s"
msgstr "vigane tüüp sättele %(setting)s asukohas %(np)s"

msgid "No database file provided"
msgstr "Puudub andmebaasifail"

#, python-format
msgid "No LDAP search base specified for %s"
msgstr "%s jaoks pole LDAP-i otsingupõhja määratud"

#, python-format
msgid "No LDAP query filter specified for %s"
msgstr "%s jaoks pole LDAP-i päringufiltrit määratud"

#, python-format
msgid "No LDAP result attribute specified for %s"
msgstr "%s jaoks pole LDAP-i tulemuse atribuuti määratud"

msgid "LDAP Scope must be one of: "
msgstr "LDAP-i ulatus peab üks järgnevatest: "

msgid "multiple_result must be one of: "
msgstr "multiple_result peab olema üks järgnevatest: "

#, python-format
msgid "Could not open %s for writing"
msgstr "%s pole võimalik kirjutamiseks avada"

#, python-format
msgid "Failed to save UserDatabase to %s"
msgstr "Kasutajate andmebaasi salvestamine asukohta %s nurjus"

#, python-format
msgid "File %s is not a profile configuration"
msgstr "Fail %s pole profiili seadistusfail"

#, python-format
msgid "Failed to add default profile %s to configuration"
msgstr "Vaikimisi profiili %s lisamine häälestusse nurjus"

#, python-format
msgid "Failed to add user %s to profile configuration"
msgstr "Kasutaja %s lisamine profiili häälestusse nurjus"

msgid "Failed to get the user list"
msgstr "Tõrge kasutajate nimekirja hankimisel"

msgid "Failed to get the group list"
msgstr "Tõrge gruppide nimekirja hankimisel"

msgid "Ignore WARNINGs"
msgstr "Eira HOIATUSi"

#, python-format
msgid "Running %s tests"
msgstr "%s testide läbiviimine"

#, python-format
msgid "Running %s tests (%s)"
msgstr "%s testide läbiviimine (%s)"

msgid "Success!"
msgstr "Edukas!"

msgid ""
"Cannot find home directory: not set in /etc/passwd and no value for $HOME in "
"environment"
msgstr ""
"Kodukataloogi pole võimalik leida: seda pole määratud ei /etc/passwd failis "
"ega ka keskkonnamuutujas $HOME"

msgid ""
"Cannot find username: not set in /etc/passwd and no value for $USER in "
"environment"
msgstr ""
"Kasutajanime pole võimalik leida: seda pole määratud ei failis /etc/passwd "
"ega ka keskkonnamuutujas $USER"

#~ msgid "Hide _menubar"
#~ msgstr "Menüüriba on _peidus"

#~ msgid "Disable lock _screen"
#~ msgstr "_Ekraani lukustamine on keelatud"

#~ msgid "Allow log _out"
#~ msgstr "_Väljalogimine on lubatud"

#~ msgid "Could not display help document '%s'"
#~ msgstr "Abidokumenti '%s' pole võimalik kuvada"

#~ msgid "Disable _unsafe protocols"
#~ msgstr "Ohtlik_ud protokollid on keelatud"

#~ msgid "Disabled Applets"
#~ msgstr "Keelatud rakendid"

#~ msgid "Lockdown Editor"
#~ msgstr "Lukustusredaktor"

#~ msgid "Safe Protocols"
#~ msgstr "Ohutud protokollid"

#~ msgid "Preferences menu"
#~ msgstr "Eelistuste menüü"

#~ msgid "Remove personal information from documents when saving them"
#~ msgstr "Dokumendi salvestamisel eemaldatakse sellest isiklikud andmed"

#~ msgid "Recommend password when saving a document"
#~ msgstr "Dokumendi salvestamisel soovitatakse parooli"

#~ msgid "Enable auto-save"
#~ msgstr "Automaatsalvestamine on lubatud"

#~ msgid "Printing should mark the document as modified"
#~ msgstr "Printimine peab dokumendi muudetuks märkima"

#~ msgid "Use system's file dialog"
#~ msgstr "Kasutatakse süsteemi failidialoogi"

#~ msgid "Create backup copy on save"
#~ msgstr "Salvestamisel luuakse varukoopia"

#~ msgid "Use OpenGL"
#~ msgstr "Kasutatakse OpenGL'i"

#~ msgid "Use system font"
#~ msgstr "Kasutatakse süsteemset kirjatüüpi"

#~ msgid "Use anti-aliasing"
#~ msgstr "Kasutatakse sakisilumist"

#~ msgid "Disable UI customization"
#~ msgstr "Kasutajaliidese kohandamine on keelatud"

#~ msgid "Show insensitive menu items"
#~ msgstr "Mittetöötavaid menüüelemente näidatakse"

#~ msgid "Show font preview"
#~ msgstr "Kirjatüübi eelvaadet näidatakse"

#~ msgid "Show font history"
#~ msgstr "Kirjatüüpide ajalugu näidatakse"

#~ msgid "Show icons in menus"
#~ msgstr "Menüüdes näidatakse ikoone"

#~ msgid ""
#~ "\n"
#~ "Automatic\n"
#~ "Large\n"
#~ "Small"
#~ msgstr ""
#~ "\n"
#~ "Automaatne\n"
#~ "Suur\n"
#~ "Väike"

#~ msgid ""
#~ "\n"
#~ "Very High\n"
#~ "High\n"
#~ "Medium\n"
#~ "Low"
#~ msgstr ""
#~ "\n"
#~ "Väga kõrge\n"
#~ "Kõrge\n"
#~ "Keskmine\n"
#~ "Madal"

#~ msgid "Default format for presentations:"
#~ msgstr "Esitluste vaikimisi vorming:"

#~ msgid "Default format for spreadsheet:"
#~ msgstr "Arvutustabelite vaikimisi vorming:"

#~ msgid "Default format for word processor:"
#~ msgstr "Tekstidokumentide vaikimisi vorming:"

#~ msgid "Default icon size"
#~ msgstr "Ikooni vaikimisi suurus"

#~ msgid "Load / Save"
#~ msgstr "Laadimine / salvestamine"

#~ msgid "Macro Security Level:"
#~ msgstr "Makrode turvatase:"

#~ msgid "Number of undo steps:"
#~ msgstr "Tagasivõetavate sammude arv:"

#~ msgid "OpenOffice.org"
#~ msgstr "OpenOffice.org"

#~ msgid "Security"
#~ msgstr "Turvalisus"

#~ msgid "User Interface"
#~ msgstr "Kasutajaliides"
